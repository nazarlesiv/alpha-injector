<?php

namespace Alpha\Injector;
use Alpha\Injector\Parameter\Parameter;
use ReflectionFunction;
use Exception;

class Injector {

  private $registry = [];

  public function __construct()
  {
    
  }

    /*
   *@access public
   *@method inject() - Invokes the supplied function with the expected parameters.
   *@param function $fn - Function into which parameters are to be injected.
   *@param Boolean $defer - If set to true, will return a function with the injected
      parameters for later executiong.
   *@return Mixed - If immediately invoked, returns the return value from the supplied
    function. Otherwise returns a function with a references to the required parameters.
   */
  public function inject($fn, $defer = false) {
    if(!is_callable($fn)) {
      throw new Exception('Invalid Function Supplied - must be callable.');
    }
    // Get the function metadata.
    $meta = new ReflectionFunction($fn);
    $params = $meta->getParameters();
    $count = count($params);
    $args = [];
    if($count) {
      for($i = 0; $i < $count; ++$i) {
        // Throw an exception if a matching module is not found?
        // or assing a NULL to the value?
        $name = $params[$i]->name;

        // Check if exists.
        if(!isset($this->registry[$name])) {
          throw new Exception("Injection Failed, cannot find '{$name}'.");
        }

        $args[] = $this->registry[$name]->value();
      }
    }
    // If the function is being deferred, return a wrapper.
    if($defer === true) {
      return function() use ($fn, $args) {
        return call_user_func_array($fn, array_merge($args, func_get_args()));
      };
    }
    return call_user_func_array($fn, $args);
  }

  public function register(Parameter $parameter): Injector {

    $name = $parameter->name();

     // Check if a value with the same name exists.
     if(isset($this->registry[$name])) {
      $value = $this->registry[$name];
      // Check if the value is not a constant.
      if($value->isConst() === true) {
        throw new Exception("Cannot override constant '{$name}'");
      }
    }
    $this->registry[$name] = $parameter;
    return $this;
  }

  public function injectables() {
    return ($temp = $this->registry);
  }

  public function get($name) {
    if(!is_string($name)) {
      return null;
    }
    return isset($this->registry[$name]) ? $this->registry[$name]->value() : null;
  }
}