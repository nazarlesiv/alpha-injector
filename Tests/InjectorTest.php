<?php

use PHPUnit\Framework\TestCase;
use Alpha\Injector\Injector;
use Alpha\Injector\Parameter\Parameter;


class InjectorTest extends TestCase
{

  protected $Injector = null;

  public static function setUpBeforeClass(): void
  {
  }

  protected function setUp(): void
  {
    $this->Injector = new Injector();
  }

  public function test_should_exist()
  {
    $this->assertNotNull($this->Injector, 'Injector object should be created.');
  }

  /**
   * @covers Injector::register
   */
  public function test_should_register_object()
  {
    $this->assertEquals(
      $this->Injector->register(new Parameter(new stdClass())),
      $this->Injector,
      'Should add a module and return a reference to self'
    );

    $this->assertEquals(
      count($this->Injector->injectables()),
      1,
      'Should return the proper count of registry items'
    );
  }


  /**
   * @covers Injector::get
   */
  public function test_should_get_object_by_name()
  {
    $obj = new stdClass();
    $this->Injector->register(new Parameter($obj));
    $this->assertEquals(
      $obj,
      $this->Injector->get(get_class($obj)),
      'Should return a proper object by name or alias'
    );
  }

  /**
   * @covers Injector::get
   */
  public function test_should_get_registered_scalar_string_value()
  {
    $name = "name";
    $value = "value";
    $this->Injector->register(new Parameter($name, $value));
    $this->assertEquals(
      $value,
      $this->Injector->get($name),
      'Should return a proper scalar value by name'
    );
  }

  /**
   * @covers Injector::get
   */
  public function test_should_get_registered_scalar_numeric_value()
  {
    $name = "name";
    $value = 5;
    $this->Injector->register(new Parameter($name, $value));
    $this->assertEquals(
      $value,
      $this->Injector->get($name),
      'Should return a proper scalar value by name'
    );
  }

  /**
   * @covers Injector::get
   */
  public function test_should_get_registered_scalar_boolean_value()
  {
    $name = "name";
    $value = false;
    $this->Injector->register(new Parameter($name, $value));
    $this->assertEquals(
      $value,
      $this->Injector->get($name),
      'Should return a proper scalar value by name'
    );
  }

  /**
   * @covers Injector::get
   */
  public function test_should_throw_exception_for_empty_name_string()
  {
    $name = "";
    $value = new stdClass();
    $this->expectException(
      Exception::class,
      'Should throw exception when adding duplicated modules'
    );
    $this->Injector->register(new Parameter($name, $value));
  }

  /**
   * @covers Injector::register
   */
  public function test_should_register_with_alias()
  {
    $obj = new stdClass();
    $alias = 'TestClass';
    $this->Injector->register(new Parameter($alias, $obj));
    $this->assertEquals(
      $obj,
      $this->Injector->get($alias),
      'Should add and retrieve a module using an alias.'
    );
  }

  /**
   * @covers Injector::register
   */
  public function test_should_not_add_duplicate_module()
  {
    $obj = new stdClass();

    $this->Injector->register(new Parameter($obj));
    $this->assertEquals(
      $obj,
      $this->Injector->get(get_class($obj)),
      'Should return a proper object by name or alias'
    );

    $this->expectException(
      Exception::class,
      'Should throw exception when adding duplicated modules'
    );
    $this->Injector->register(new Parameter($obj));
  }

  /**
   * @covers Injector::inject
   */
  public function test_should_inject_values()
  {
    $Injector = $this->Injector;
    $objVal = new stdClass();
    $stringVal = 'valTwo';

    $injectedFunction = function ($objVal, $stringVal) use ($Injector) {
      $this->assertEquals(
        $Injector->get('objVal'),
        $objVal,
        'Should Inject a object value'
      );
      $this->assertEquals(
        $Injector->get('stringVal'),
        $stringVal,
        'Should Inject a string value'
      );
    };

    $Injector->register(new Parameter('objVal', $objVal));
    $Injector->register(new Parameter('stringVal', $stringVal));
    $Injector->inject($injectedFunction);
  }

  /**
   * @covers Injector::inject
   */
  public function test_should_defer_injected_function_execution()
  {
    $Injector = $this->Injector;
    $objVal = new stdClass();
    $stringVal = 'valTwo';

    $injectedFunction = function ($objVal, $stringVal) use ($Injector) {
      $this->assertEquals(
        $Injector->get('objVal'),
        $objVal,
        'Should Inject a object value'
      );
      $this->assertEquals(
        $Injector->get('stringVal'),
        $stringVal,
        'Should Inject a string value'
      );
    };

    $Injector->register(new Parameter('objVal', $objVal));
    $Injector->register(new Parameter('stringVal', $stringVal));

    $deferred = $Injector->inject($injectedFunction, true);

    $this->assertTrue(
      is_callable($deferred),
      'Should return a callable wrapper.'
    );

    $deferred();
  }

  /**
   * @covers Injector::inject
   */
  public function test_should_throw_exception_for_missing_value()
  {
    $Injector = $this->Injector;
    $objVal = new stdClass();
    $stringVal = 'valTwo';

    $Injector->register(new Parameter($objVal));
    $Injector->register(new Parameter('stringVal', $stringVal));

    $injectedFunction = function ($objVal, $stringVal) use ($Injector) {
    };

    $this->expectException(
      Exception::class,
      'Should throw exception if value does not exist'
    );
    $Injector->inject($injectedFunction);
  }
}
