<?php

use PHPUnit\Framework\TestCase;
use Alpha\Injector\Parameter\Parameter;


class ParameterTest extends TestCase
{

  protected $Parameter = null;

  public static function setUpBeforeClass(): void
  {
  }

  protected function setUp(): void
  {
    $this->Parameter = new Parameter("test");
  }

  public function test_should_exist()
  {
    $this->assertNotNull($this->Parameter, 'Injector object should be created.');
  }
}
