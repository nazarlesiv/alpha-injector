<?php

namespace Alpha\Injector\Parameter;

use Exception;

class Parameter {

  private $name = '';
  private $value;

  private $const = true;

  
  public function __construct($name, $value = null)
  {

    // If a name string was supplied and its empty, throw error
    if(is_string($name) && empty($name)) {
      throw new Exception("Cannot supply empty string as Parameter name.");
    }
    /*
     If the value parameter is not supplied, assume the object is supplied as the first parameter
     */
    if(is_null($value)) {
      // Set the name parameter as the value.
      $value = $name;
      
      // Get the class name of the supplied object as the name field.
      $name = $this->getObjectAlias($name);
    }
    
    $this->name = $name;
    $this->value = $value;
  }

  private function getObjectAlias($object): string {
    if(is_scalar($object)) {
      return strval($object);
    } else if(is_object($object)) {
      //return get_class($object);
      return (new \ReflectionClass($object))->getShortName();
    }
  }

  public function name($name = null) {
    if(is_null($name)) {
      return $this->name;
    } else {
      $this->name = $name;
    }
  }

  public function value($value = null) {
    if(is_null($value)) {
      return $this->value;
    } else {
      $this->value = $value;
    }
  }

  public function const(bool $value) {
    $this->const = $value;
    return $this;
  }
  public function isConst(): bool {
    return $this->const;
  }
}